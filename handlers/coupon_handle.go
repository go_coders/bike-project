package handlers

import (
	"fmt"
	"github.com/bike_project/db"
	"github.com/bike_project/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	//"time"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

//Bill Calculated
//Coupon name
//Email Required
type TmpStruct struct {
	BillCalculated float64 `json:"bill"`
	Email          string  `json:"email"`
	Cpname         string  `json:"cpname"`
}

func CreateCoupon(c *gin.Context) {
	conn := db.Session.DB("start").C(db.CollectionCoupon)
	var temp_coupon models.Coupon
	c.Bind(&temp_coupon)
	fmt.Println("The entered database %v \n", temp_coupon)

	err := conn.Insert(temp_coupon)
	if err != nil {
		fmt.Println("Dude , there is database error")
		return
	}
}

func CouponUse(c *gin.Context) {
	var tmp TmpStruct
	c.Bind(&tmp)
	conn1 := db.Session.DB("start").C(db.CollectionUserCoupon)
	conn2 := db.Session.DB("start").C(db.CollectionCoupon)
	bill := tmp.BillCalculated
	var tmpCoupon models.Coupon
	var tmpUserCoupon models.UserCoupon
	err := conn2.Find(bson.M{"cpname": tmp.Cpname}).One(&tmpCoupon)
	/*etime := time.Now().UnixNano() / int64(time.Millisecond)
	if etime > tmpCoupon.Ettime {
		c.JSON(http.StatusOK, gin.H{
			"message": "The timing for the message has been expired",
		})
		return
	}*/
	if err != nil {
		failOnError(err, "We did not find this type of Coupon")
		c.JSON(http.StatusConflict, gin.H{
			"message": err,
			"msg2":    "Check your Response",
		})
		return
	}
	err = conn1.Find(bson.M{"email": tmp.Email}).One(&tmpUserCoupon)
	fmt.Println("The tmpUserCoupon is %v", tmpUserCoupon)
	emptyStruct := models.UserCoupon{}
	if tmpUserCoupon == emptyStruct {
		fmt.Println("Im inside this if")
		tmp2 := models.UserCoupon{
			Email:  tmp.Email,
			Cpname: tmp.Cpname,
			Freq:   0,
		}
		fmt.Println("The inserting document is %v", tmp2)
		err = conn1.Insert(tmp2)
		if err != nil {
			failOnError(err, "Again the database error")
			return
		}

		disc := tmpCoupon.Discount
		net_subtract := (bill * disc) / 100
		bill = bill - net_subtract
		c.JSON(http.StatusOK, gin.H{
			"UpdatedBill": bill,
			"message":     "Everything is Okay",
		})
	} else {
		fre_check := tmpCoupon.Freq
		ac_cnt := tmpUserCoupon.Freq
		if ac_cnt >= fre_check-1 {
			c.JSON(http.StatusOK, gin.H{
				"message": "You have already used the coupons",
			})
			return
		}
		//	var tmp2 models.UserCoupon
		up_freq := ac_cnt + 1
		fmt.Println("The email is %s", tmpUserCoupon.Email)
		query := bson.M{"email": tmpUserCoupon.Email}
		fmt.Println("The Frequency is %v ", up_freq)
		upd := bson.M{
			"email":  tmpUserCoupon.Email,
			"cpname": tmpUserCoupon.Cpname,
			"freq":   up_freq,
		}
		err = conn1.Update(query, upd)
		if err != nil {
			failOnError(err, "Unable to Update")
			//c.Error(err)
		}
		disc := tmpCoupon.Discount
		net_subtract := (bill * disc) / 100
		bill = bill - net_subtract
		c.JSON(http.StatusOK, gin.H{
			"UpdatedBill": bill,
			"message":     "Everything is Okay",
		})
	}

}
