package handlers

import (
	//"encoding/json"
	"fmt"
	"github.com/bike_project/db"
	"github.com/bike_project/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	//"github.com/alexjlockwood/gcm"
	//"log"
	//"time"
)

var globalId int64

func initialize() {
	globalId = 0
}

func getId() int64 {
	globalId = globalId + 1
	return globalId
}

func ManagerLogin(c *gin.Context) {
	managerInstance := models.StoreManager{}
	managerConnection := db.Session.DB("start").C(db.CollectionStoreManager)
	err := c.Bind(&managerInstance)
	if err != nil {
		c.JSON(200, gin.H{
			"error": "Binding Error",
		})
		return
	}
	fmt.Println("%v", managerInstance)
	manager := models.StoreManager{}
	err = managerConnection.Find(bson.M{"loginid": managerInstance.LoginID}).One(&manager)
	if err != nil {
		failOnError(err, "Database error line 40")
		c.JSON(200, gin.H{"error": "line 41"})
	}
	fmt.Println("%v", managerInstance.Password)
	fmt.Println("%v", manager.Password)
	if managerInstance.Password != manager.Password {
		c.JSON(200, gin.H{"error": "Authentication Error"})
		fmt.Println("Authentication Error line 45")
	} else {
		c.JSON(200, gin.H{
			"success": "store manager logged in",
		})
		fmt.Println("Store manager is logged in!!!")
	}

}

func getBill(ride models.Ride) int64 {
	return 12
}

//ServerApiKey: AIzaSyBt64IQRg8yczb1nQEEzZE5bEEzKfaz680
const (
	Stmname = "priya"
)

func IsLoggedIn(auth int64) bool {
	userConnection := db.Session.DB("start").C(db.CollectionUser)
	user := models.User{}
	err := userConnection.Find(bson.M{"created_at": auth}).One(&user)
	if err != nil {
		failOnError(err, "Error in database query or invalid auth")
		return false
	}
	return user.IsActive
}

func getRide(Id int64) models.Ride {
	rideConnection := db.Session.DB("start").C(db.CollectionRide)
	ride := models.Ride{}
	err := rideConnection.Find(bson.M{"id": Id}).One(&ride)
	if err != nil {
		failOnError(err, "Database error")
		return models.Ride{}
	}
	return ride
}

func getUser(auth int64) models.User {
	userConnection := db.Session.DB("start").C(db.CollectionUser)
	user := models.User{}
	err := userConnection.Find(bson.M{"created_at": auth}).One(&user)
	fmt.Println("%v", user)
	if err != nil {
		failOnError(err, "Database Error")
		return models.User{}
	}
	return user
}
func RemoveUser(auth int64) {
	userConnection := db.Session.DB("start").C(db.CollectionUser)
	err := userConnection.Remove(bson.M{"created_at": auth})
	if err != nil {
		failOnError(err, "Failing at removing line 103")
	}
}

func DeliveryRequest(c *gin.Context) {
	//Get the Request
	request := models.DeliveryReq{}
	err := c.Bind(&request)
	fmt.Println("%v", request)
	if err != nil {
		failOnError(err, "Error in Binding")
		return
	}
	//Get the user
	user := models.User{}
	user = getUser(request.Auth)
	fmt.Println("%v", user)
	//Validate it
	if user.IsActive == true && user.IsRunning == false {
		//Find the nearest Store Manager and its Store Manager
		stmInstance := models.StoreManager{}
		userConnection := db.Session.DB("start").C(db.CollectionUser)
		managerConnection := db.Session.DB("start").C(db.CollectionStoreManager)
		storeConnection := db.Session.DB("start").C(db.CollectionStore)

		store := models.Store{}
		err = storeConnection.Find(bson.M{"stmid": Stmname}).One(&store)
		if err != nil {
			failOnError(err, "There was a database error")
			c.JSON(200, gin.H{
				"error": "Error in connection",
			})
			return
		}
		err = managerConnection.Find(bson.M{"loginid": Stmname}).One(&stmInstance)
		if err != nil {
			failOnError(err, "There was a database error")
			c.JSON(200, gin.H{
				"error": "Error in connection",
			})
			return
		}
		fmt.Println("%v", stmInstance)

		// Assign the bike
		/*
			bikearray := store.BikeId
			user.IsRunning = true
			if len(bikearray) != 0 {
				user.Bikeassign = bikearray[0]
				store.BikeId = bikearray[0:]
			} else {
				c.JSON(200, gin.H{"error": "Bike not available in this store"})
				return
			}
		*/

		//Formating to string
		lat_to_send := strconv.FormatFloat(float64(request.Lat), 'f', 13, 32)
		lon_to_send := strconv.FormatFloat(float64(request.Lon), 'f', 13, 32)
		type_of_request := strconv.Itoa(request.TypeofRequest)
		auth_to_send := strconv.FormatInt(request.Auth, 10)
		//Notify nearest store manager
		data := map[string]interface{}{"name": user.Name, "phone": user.Mobno, "lat": lat_to_send, "lon": lon_to_send, "typeofrequest": type_of_request, "auth": auth_to_send}
		gcmSend(stmInstance.DeviceId, stmInstance.ServerApiKey, data)
		//Send the Response to the user

		user.IsRunning = true

		RemoveUser(request.Auth)
		err = userConnection.Insert(user)
		dummyUser := getUser(request.Auth)
		fmt.Println("%v", dummyUser)
		fmt.Println("Updated the user")
		if err != nil {
			failOnError(err, "Unable to update the database")
		}
		c.JSON(200, gin.H{
			"available": "Please Wait for sometime",
		})
		return
	} else {
		c.JSON(
			200, gin.H{
				"wrong": "Not Authentic User or already booked a ride",
			})
		return
	}
}
func Limage(c *gin.Context) {
	rideConnection := db.Session.DB("start").C(db.CollectionRide)
	conn := db.Session.DB("start").C(db.CollectionLicenseImage)
	ride := getRide(globalId)
	tride := models.Ride{}
	err := c.Bind(&tride)
	if err != nil {
		failOnError(err, "line 198")
	}
	err = conn.Insert(tride)
	query := bson.M{"id": ride.Id}
	upd := bson.M{"$set": bson.M{
		"limage": tride.LicenseImage,
	}}
	err = rideConnection.Update(query, upd)
	if err != nil {
		failOnError(err, "Error 207")
	}
}

func StMeterImage(c *gin.Context) {
	rideConnection := db.Session.DB("start").C(db.CollectionRide)
	conn := db.Session.DB("start").C(db.CollectionStartMeterImage)
	ride := models.Ride{}
	err := c.Bind(&ride)
	if err != nil {
		failOnError(err, "Line 201")
		return
	}
	err = conn.Insert(ride)
	ride.Id = getId()
	err = rideConnection.Insert(ride)
	if err != nil {
		failOnError(err, "line 207")
		return
	}
	fmt.Println("%v", ride)
	fmt.Println("Ride Successfully Created")
}
func EMeterImage(c *gin.Context) {
	rideConnection := db.Session.DB("start").C(db.CollectionRide)
	conn := db.Session.DB("start").C(db.CollectionEndMeterImage)
	ride := models.Ride{}
	err := c.Bind(&ride)
	err = conn.Insert(ride)
	if err != nil {
		failOnError(err, "line 232")
	}

	query := bson.M{"id": ride.Id}
	upd := bson.M{"$set": bson.M{
		"emeterimage": ride.EndMeterImage,
	}}
	err = rideConnection.Update(query, upd)
	if err != nil {
		failOnError(err, "line 241")
	}
}

//Ride Confirm By Store manager
func RideConfirm(c *gin.Context) {
	//Create the Ride Connection
	rideConnection := db.Session.DB("start").C(db.CollectionRide)
	storeConnection := db.Session.DB("start").C(db.CollectionStoreManager)

	ride := models.Ride{}
	err := c.Bind(&ride)
	if err != nil {
		failOnError(err, "Binding Error")
		return
	}
	store := models.Store{}
	err = storeConnection.Find(bson.M{"stmid": Stmname}).One(&store)
	if err != nil {
		failOnError(err, "Database error")
	}
	//Get the user
	user := models.User{}
	user = getUser(ride.Auth)

	bikearray := store.BikeId

	if len(bikearray) != 0 {
		user.Bikeassign = bikearray[0]
		store.BikeId = bikearray[0:]
	} else {
		c.JSON(200, gin.H{
			"wait": "Please wait for sometimes",
		})
		return
	}
	//Sending data to a Particular user
	data := map[string]interface{}{"success": "Your Ride has been started"}
	gcmSend(user.DeviceId, user.ServerApiKey, data)
	//rideStart.Id = getId()
	query := bson.M{"id": globalId}
	upd := bson.M{"$set": bson.M{
		"auth":      ride.Auth,
		"sttime":    ride.StartTime,
		"streading": ride.StartMeterReading,
	}}
	err = rideConnection.Update(query, upd)
	if err != nil {
		failOnError(err, "Error in inserting in database")
		c.JSON(200, gin.H{
			"error": "there was some error",
		})
	} else {
		c.JSON(200, gin.H{
			"success": "Successfully Updated",
			"id":      globalId,
		})
		fmt.Println("The Ride has been Confirmed")
	}
}

//Stop Request issued from user app
func StopRequest(c *gin.Context) {
	//userConnection := db.Session.DB("start").C(db.CollectionUser)

	request := models.DeliveryReq{}
	err := c.Bind(&request)
	if err != nil {
		failOnError(err, "Binding Error")
		return
	}

	user := models.User{}
	user = getUser(request.Auth)
	if user.IsActive {
		stmInstance := models.StoreManager{}
		managerConnection := db.Session.DB("start").C(db.CollectionStoreManager)
		err = managerConnection.Find(bson.M{"loginid": Stmname}).One(&stmInstance)
		if err != nil {
			failOnError(err, "Database error")
			return
		}
		//Formating to string
		lat_to_send := strconv.FormatFloat(float64(request.Lat), 'f', 13, 32)
		lon_to_send := strconv.FormatFloat(float64(request.Lon), 'f', 13, 32)
		type_of_request := strconv.Itoa(request.TypeofRequest)
		auth_to_send := strconv.FormatInt(request.Auth, 10)
		//Notify nearest store manager
		data := map[string]interface{}{"name": user.Name, "phone": user.Mobno, "lat": lat_to_send, "lon": lon_to_send, "typeofrequest": type_of_request, "auth": auth_to_send}
		gcmSend(stmInstance.DeviceId, stmInstance.ServerApiKey, data)
		c.JSON(200, gin.H{"stop": "Your Ride has been stopped"})
	} else {
		c.JSON(
			200, gin.H{
				"wrong": "Not Authentic User",
			})
		return
	}
}
func EndMeterImage(c *gin.Context) {
	conn := db.Session.DB("start").C(db.CollectionEndMeterImage)
	eimage := models.Emeter{}
	err := c.Bind(&eimage)
	if err != nil {
		failOnError(err, "line 344")
	}
	err = conn.Insert(eimage)
	if err != nil {
		failOnError(err, "line 349")
	}
}

//StopConfirm issued by store manager
func StopConfirm(c *gin.Context) {
	rideConnection := db.Session.DB("start").C(db.CollectionRide)

	stpConfirm := models.StopConfirm{}
	err := c.Bind(&stpConfirm)
	if err != nil {
		failOnError(err, "Binding Error")
		return
	}
	user := models.User{}
	user = getUser(stpConfirm.Auth)
	if user.IsActive && user.IsRunning {
		// Update the Ride
		query := bson.M{"id": stpConfirm.Id}
		upd := bson.M{"$set": bson.M{
			"etime":    stpConfirm.EndTime,
			"ereading": stpConfirm.EndReading,
		}}
		err = rideConnection.Update(query, upd)
		if err != nil {
			failOnError(err, "Unable to Update")
			return
		}
		//Get the Ride
		ride := getRide(stpConfirm.Id)
		bill := getBill(ride)
		query = bson.M{"id": stpConfirm.Id}
		upd = bson.M{"$set": bson.M{
			"bill": bill,
		}}
		err = rideConnection.Update(query, upd)
		data := map[string]interface{}{"success": "Your Ride has been stopped"}
		gcmSend(user.DeviceId, user.ServerApiKey, data)
		c.JSON(200, gin.H{"bill": bill})
		user.IsRunning = false
	} else {
		c.JSON(200, gin.H{"wrong": "Authentication error"})
		return
	}
}

/*
func StoreConfirm(c *gin.Context) {
	request := models.DeliveryReq{}
	err := c.Bind(&request)
	if err != nil {
		failOnError(err, "Error in Binding")
		return
	}

	if IsLoggedIn(request.Auth) {
		stmInstance := models.StoreManager{}
		log.Println("%v", stmInstance)
		//err = storemanagerConnection.Find(bson.M{"" : }).One(&stmInstance)
		//Find the nearby Store Manager
		//Send through the GCM
		data := map[string]interface{}{"name": "How you doing?"}
		gcmSend(stmInstance.DeviceId, stmInstance.ServerApiKey, data)
		c.JSON(200, gin.H{
			"available": "Please Wait for sometimes",
		})
		return
	} else {
		c.JSON(
			200, gin.H{
				"Wrong": "Not Authentic User",
			})
		return
	}
}
*/
