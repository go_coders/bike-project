package handlers

import (
	"fmt"
	"github.com/bike_project/db"
	"github.com/bike_project/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"log"
	"regexp"
	//"strconv"
	"github.com/alexjlockwood/gcm"
	"time"
)

const (
	UserApikey  = "AIzaSyBCBT-lTty50MN5_UoTabXd8eZfcKPIM4Q"
	StoreApikey = "AIzaSyAHBnGHw3JTpoIolsS12r_PjvKLH9SBccQ"
)

func validateEmail(email string) bool {
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return Re.MatchString(email)
}
func gcmSend(regIDs string, apkey string, data map[string]interface{}) {
	//dummy := models.Dummy
	//data := map[string]interface{}{"score": "Brother !! How you doing?"}
	msg := gcm.NewMessage(data, regIDs)

	// Create a Sender to send the message.
	sender := &gcm.Sender{ApiKey: apkey}

	// Send the message and receive the response after at most two retries.
	response, err := sender.Send(msg, 2)
	if err != nil {
		fmt.Println("Failed to send message:", err)
		return
	}
	fmt.Println(response)
}
func SignUp(c *gin.Context) {
	//connection variable for connecting to database
	connection := db.Session.DB("start").C(db.CollectionUser)
	usertocreate := models.User{}
	//Bind the user
	err := c.Bind(&usertocreate)
	if err != nil {
		failOnError(err, "There is some database error")
		return
	}

	//Check if already exists
	emptyStruct := models.User{}
	var dummy models.User
	err = connection.Find(bson.M{"email": usertocreate.Email}).One(&dummy)
	if dummy == emptyStruct {
		log.Println("Unique user")
	} else {
		log.Println("Already a exising user")
		userauth := fmt.Sprintf("%v", dummy.CreatedAt)
		c.JSON(200, gin.H{
			"Already": "Exisiting user",
			"Auth":    userauth,
		})
		return
	}
	//Append timestamps
	usertocreate.CreatedAt = time.Now().UnixNano() / int64(time.Millisecond)
	usertocreate.UpdatedAt = time.Now().UnixNano() / int64(time.Millisecond)
	usertocreate.IsActive = true
	usertocreate.IsRunning = false
	usertocreate.Bikeassign = 0
	usertocreate.ServerApiKey = UserApikey
	//genrate auth key
	//auth := fmt.Sprintf("%v", usertocreate.CreatedAt)
	auth := usertocreate.CreatedAt
	fmt.Println("%v", usertocreate)
	err = connection.Insert(usertocreate)
	if err != nil {
		failOnError(err, "Some database error 79")
	}
	//log.Println("Testing")
	log.Println("%v", auth)
	c.JSON(200, gin.H{
		"Success": "Successfully Created user",
		"Auth":    auth,
	})
	fmt.Println("User has been successfully Created")
	//display_users()
}

func Login(c *gin.Context) {
	connection := db.Session.DB("start").C(db.CollectionUser)
	var trylogin models.UserLogin
	err := c.Bind(&trylogin)
	if err != nil {
		failOnError(err, "Binding Error")
		return
	}

	user := models.User{}
	err = connection.Find(bson.M{"auth": trylogin.Auth}).One(&user)
	if err != nil {
		failOnError(err, "Something wrong with connection")
		return
	}
	if user.Password != trylogin.Password || trylogin.Email != user.Email {
		c.JSON(200, gin.H{
			"Wrong": "Wrong email or password",
		})
		return
	} else {
		c.JSON(200, gin.H{
			"Success": "Give access",
		})
		user.IsActive = true
	}

}

func Logout(c *gin.Context) {
	obj := models.UserLogout{}
	err := c.Bind(&obj)
	if err != nil {
		failOnError(err, "Binding error")
	}
	connection := db.Session.DB("start").C(db.CollectionUser)
	var user models.User
	err = connection.Find(bson.M{"created_at": obj.Auth}).One(&user)
	log.Println("The logging out user is %v", user)
	user.IsActive = false
	log.Println("The logging out user is %v", user)
}

func Send1(c *gin.Context) {
	data := map[string]interface{}{"score": "Hello", "name": "Priya", "phone": "99934930243", "lat": "18.945345", "lon": "20.67", "typeofrequest": "0", "auth": "95685386396"}
	//regIDs := "crsGfcbNrJQ:APA91bHCgS3R0cxp5hXx1W29H83MfOBDWgcRKA_Duc2Q7LAEOZGcIkW6v74Q_AbwWgj0HPe-DS3kWqcxGew9m8JqQGEXlgRYinyclBTpNU9APKyzAhD5pV_XBQ4DJ__ucKIge5RXjgO5"
	reg2 := "cMcpSJzk80U:APA91bEpT9d0jF4UGRUUIWzLipIqwZiL1rycbtAqqyZqfekLv4hiN0ak9lUNMW_v2LuU_dIjaTejwYPodELr395Cd2Cetj5xtLnijoFkoAMJuhaoiVLH1ruotg7pv2rRpDjvOY3kmwwL"
	gcmSend(reg2, UserApikey, data)
	c.String(200, "The message is sent")
}

func Send(c *gin.Context) {
	//dummy := models.Dummy
	data := map[string]interface{}{"score": "Hello", "name": "Andrew", "phone": "99934930243", "lat": "18.519569", "lon": "73.855347", "typeofrequest": "1", "auth": "95685386396"}
	regIDs := " eL7WdxV_DHA:APA91bEZqQ-7_jrDSryNpWWHLl-hcM3OmYt5h5alLBmT2op8dQLLSHHNxIcMqfsiBQsqurd4uvB6quDH8B2Sgn67-L4w7UJNpwQsiqm2bbYZ9qwAy7n6I1K3EeCGYez3372hZVmeqpSy"
	//reg2 := "cMcpSJzk80U:APA91bEpT9d0jF4UGRUUIWzLipIqwZiL1rycbtAqqyZqfekLv4hiN0ak9lUNMW_v2LuU_dIjaTejwYPodELr395Cd2Cetj5xtLnijoFkoAMJuhaoiVLH1ruotg7pv2rRpDjvOY3kmwwL"
	gcmSend(regIDs, StoreApikey, data)
	c.String(200, "The message is sent")
}

func Displayusers(c *gin.Context) {
	conn := db.Session.DB("start").C(db.CollectionUser)
	var user models.User
	iter := conn.Find(nil).Iter()
	for iter.Next(&user) {
		//fmt.Println("%v", user.Id)
		fmt.Println("The user printed is %v \n", user)
		c.JSON(200, gin.H{
			"Auth": string(user.Id),
		})
	}
}

func Pinga(c *gin.Context) {
	c.String(200, "Just pinging")
}
