package db

import (
	"fmt"
	//"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"os"
)

var (
	Session *mgo.Session
	Mongo   *mgo.DialInfo
)

const (
	MongoDBUrl                = "mongodb://localhost:27017/start"
	CollectionUser            = "users"
	CollectionStore           = "stores"
	CollectionRide            = "ride"
	CollectionStoreManager    = "storemanagers"
	CollectionStartRide       = "startride"
	CollectionEndRide         = "endride"
	CollectionCoupon          = "coupons"
	CollectionUserCoupon      = "usercoupons"
	CollectionEndMeterImage   = "eimage"
	CollectionStartMeterImage = "simage"
	CollectionLicenseImage    = "limage"
	Port                      = "7000"
)

func Connect() {
	uri := os.Getenv("MONGODB_URL")
	if len(uri) == 0 {
		uri = MongoDBUrl
	}
	mongo, err := mgo.ParseURL(uri)
	if err != nil {
		fmt.Println("There is some error")
		return
	}

	s, err := mgo.Dial(uri)
	s.SetSafe(&mgo.Safe{})
	if err != nil {
		fmt.Println("There i some error")
		return
	}
	Session = s
	Mongo = mongo
}
