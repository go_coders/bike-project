cd db
go build
go install
cd ..

cd models
go build
go install
cd ..

cd handlers
go build
go install
cd ..

cd cmd
go build
go install
cd ..

