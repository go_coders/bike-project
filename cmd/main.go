package main

import (
	"fmt"
	"github.com/bike_project/db"
	"github.com/bike_project/handlers"

	"github.com/gin-gonic/gin"
	"os"
)

func main() {
	// Do the database connection
	db.Connect()
	fmt.Println("Server is going to start")
	router := gin.Default()

	router.GET("/", handlers.Pinga)
	router.POST("/Signup", handlers.SignUp)
	router.POST("/CouponUse", handlers.CouponUse)
	router.POST("/CreateCoupon", handlers.CreateCoupon)
	router.POST("/Logout", handlers.Logout)
	router.GET("/Display", handlers.Displayusers)
	router.GET("/Send", handlers.Send)
	router.GET("/Send1", handlers.Send1)

	//Store Related
	router.POST("/ManagerLogin", handlers.ManagerLogin)

	router.POST("/DeliveryRequest", handlers.DeliveryRequest)
	router.POST("/RideConfirm", handlers.RideConfirm)
	router.POST("/EndMeterImage", handlers.EMeterImage)
	router.POST("/StMeterImage", handlers.StMeterImage)
	router.POST("/Limage", handlers.Limage)
	router.POST("/StopRequest", handlers.StopRequest)
	router.POST("/StopConfirm", handlers.StopConfirm)
	//router.POST("/CreateStore", handlers.CreateStore)

	port := db.Port
	if len(os.Getenv("PORT")) > 0 {
		port = os.Getenv("PORT")
	}
	router.Run(":" + port)
}
