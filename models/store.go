package models

import (
	"fmt"
	"github.com/bike_project/db"
	"gopkg.in/mgo.v2/bson"
)

type Store struct {
	ID       int       `bson:"id" json:"id" `
	Name     string    `bson:"name" json:"name" binding:"required"`
	Loc      []float64 `bson:"loc" json:"loc"		 binding:"required"`
	NoBikes  int       `bson:"nbikes" json:"nbikes" binding:"required"`
	BikeId   []int64   `bson:"bid" json:"bid" binding:"required"`
	StoreMID string    `bson:"stmid" json:"stmid" binding:"required"`
}

type StoreManager struct {
	LoginID      string `bson:"loginid" json:"loginid" binding:"required"`
	Password     string `bson:"passwd" json:"passwd" binding:"required"`
	ServerApiKey string `bson:"serverapikey,omitempty" json:"serverapikey,omitempty"`
	DeviceId     string `bson:"deviceid,omitempty" json:"deviceid,omitempty"`
}
type DeliveryReq struct {
	Auth          int64   `json:"auth"`
	Lat           float64 `json:"lat"`
	Lon           float64 `json:"lon"`
	TypeofRequest int     `json:"typeofrequest"`
}

func GetStoreInfo(id int) Store {
	var temp_store Store
	conn := db.Session.DB("start").C(db.CollectionStore)
	err := conn.Find(bson.M{"id": id}).One(&temp_store)

	if err != nil {
		fmt.Println("There is a database error \n")
		return temp_store
	}
	return temp_store
}

/*
//Find nearby Store
func (sm *Store) FindNearByStore(Lon float64, Lat float64) (Store, error) {
	storeConnection := db.Session.DB("start").C(db.CollectionStore)
	var result Store

	pipeline := bson.M{
		"loc": bson.M{
			"$near": []float64{Lon, Lat,},
			"$maxDistance": 200
		},
	}
	// log.Println(pipeline)
	err := storeConnection.Find(pipeline).One(&result)
	if err != nil {
		log.Println(err)
	}
	return result, err
}
*/
