package models

import (
//"time"
)

type Ride struct {
	Id                int64  `bson:"id" json:"id"`
	Auth              int64  `bson:"auth" json:"auth"`
	StartMeterReading int64  `bson:"streading" json:"streading"`
	StartMeterImage   string `bson:"stmeterimage" json:"stmeterimage"`
	EndMeterReading   int64  `bson:"ereading" json:"ereading"`
	EndMeterImage     string `bson:"emeterimage" json"emeterimage"`
	StartTime         int64  `bson:"sttime" json:"sttime"`
	EndTime           int64  `bson:"etime" json:"etime"`
	LicenseImage      int64  `bson:"limage" json:"limage"`
	BikeId            int64  `bson:"bids" json:"bids"`
	Bill              int64  `bson:"bill" json:"bill"`
}

type Emeter struct {
	EndMeterImage string `bson:"emeterimage" json:""emeterimage`
}
type Smeter struct {
	StartMeterImage string `bson:"stmeterimg" json:"stmeterimg"`
}
type Limage struct {
	LicenseImage string `bson:"limage" json:"limage"`
}
type StopConfirm struct {
	Id         int64 `bson:"id" json:"id"`
	EndTime    int64 `bson:"etime" json:"etime"`
	EndReading int64 `bson:"ereading" json:"ereading"`
	Auth       int64 `bson:"auth" json:"auth"`
}

//Issued by Store Manager
type RideStart struct {
	Id                int   `bson:"id,omitempty" json:"id,omitempty"`
	StartMeterReading int64 `bson:"streading" json:"streading"`
	StartTime         int64 `bson:"sttime" json:"sttime"`
}

/*
type StartRide struct {
	StartReading int       `bson:"streading" json:"streading" binding:"required"`
	BikeId       int       `bson:"bids" json:"bids" binding:"required"`
	Mobno        string    `bson:"mobno" json:"mobno" binding:"required"`
	StoreId      int       `bson:"sid" json:"sid" binding:"required"`
	StartTime    time.Time `bson:"sttime" json:"sttime" binding:"required"`
}

type EndRide struct {
	EndReading int       `bson:"ereading" json:"ereading" binding:"required"`
	BikeId     int       `bson:"bids" json:"bids" binding:"required"`
	Mobno      string    `bson:"mobno" json:"mobno" binding:"required"`
	StoreId    int       `bson:"sid" json:"sid" binding:"required"`
	EndTime    time.Time `bson:"etime" json:"etime" binding:"required"`
}
*/
