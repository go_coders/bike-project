package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Coupon struct {
	CId      bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Cpname   string        `bson:"cpname" json:"cpname"`
	Sttime   int64         `bson:"sttime" json:"sttime"`
	Ettime   int64         `bson:"ettime" json:"ettime"`
	Discount float64       `bson:"disc" json:"disc"`
	Freq     int64         `bson:"freq" json:"freq" binding:"required"`
}

type UserCoupon struct {
	Email  string `bson:"email" json:"email"`
	Cpname string `bson:"cpname" json:"cpname"`
	Freq   int64  `bson:"freq" json:"freq"`
}
