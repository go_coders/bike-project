package models

import (
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`

	Name      string `bson:"name" json:"name" binding:"required"`
	Password  string `bson:"pass" json:"pass" binding:"required"`
	Email     string `bson:"email" json:"email" binding:"required"`
	Mobno     string `bson:"mobno" json:"mobno" binding:"required"`
	CreatedAt int64  `bson:"created_at" json:"created_at" `
	UpdatedAt int64  `bson:"updated_at" json:"updated_at" `

	// Extra info
	IsRunning    bool   `bson:"isrunning" json:"isrunning"`
	Bikeassign   int64  `bson:"bikeassign" json:"bikeassign"`
	SignupType   int64  `bson:"signuptype" json:"signuptype"`
	IsActive     bool   `bson:"isactive"`
	ServerApiKey string `bson:"serverapikey" json:"serverapikey"`
	DeviceId     string `bson:"deviceid" json:"deviceid"`
}

type UserLogout struct {
	Auth int64 `json:"auth"`
}
type UserLogin struct {
	Auth     int64  `json:"auth"`
	Email    string `json:"email"`
	Password string `json:"pass"`
}
type Dummy struct {
	DeviceId string `json:"deviceid"`
}
